import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

let service: AxiosInstance | null = null;

export const installAxios = (options: AxiosRequestConfig) => {
  service = axios.create(options);
};

export const getAxiosService = (options: AxiosRequestConfig) => {
  if (!service) service = axios.create();
  return service(options);
};
