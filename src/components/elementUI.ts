/**
 * ElementUI 定义文件
 */

/**
 * ID
 */
export const ID = 'id';

/**
 * 大小
 */
export type Size = 'default' | 'small' | 'large';
/**
 * 根据Size选择Font
 */
export const sizeToFont: (size?: Size) => number = (size) => {
  switch (size) {
    case 'small':
      return 12;
    case 'large':
      return 18;
    default:
      return 14;
  }
};

/**
 * 外观颜色
 */
export type Face = '' | 'success' | 'warning' | 'danger' | 'info';

export const faces: Array<Face> = ['danger', 'success', '', 'warning', 'info'];
/**
 * Face to Color
 */
export const faceToColor: (face?: Face) => string = (face) => {
  switch (face) {
    case 'danger':
      return '#F56C6C';
    case 'success':
      return '#67C23A';
    case 'warning':
      return '#E6A23C';
    case 'info':
      return '#909399';
    default:
      return '#409EFF';
  }
};

/**
 * 对齐
 */
export type Align = 'left' | 'center' | 'right';

/**
 * 图标
 */
export type Icon = string;

/**
 * 基础定义
 */
export interface Er {
  /**
   * 元数据索引
   */
  name: string;
  /**
   * 标签
   */
  label?: string | number;

  /**
   * 图标
   */
  icon?: Icon;
  /**
   * 大小
   */
  size?: Size;
}

export interface Erx extends Er {
  [p: string]: unknown;
}

/**
 * 行数据
 */
export type Row = {
  [ID]?: number | string;
  [p: string]: unknown;
};

export type WhenValue = boolean | string[] | number[];

/**
 * 条件信息
 */
export type When = {
  /**
   * true: display
   */
  [name: string]: WhenValue;
};
/**
 * 可以disable的组件配置项
 */
export interface DisabledComponent {
  disabled?: boolean;
  when?: When;
}
/**
 * 计算Disabled
 */
export function isDisabled(obj: DisabledComponent, row?: Row) {
  if (obj.disabled) return true;

  if (!(row && obj.when)) return false;

  const keys = Object.keys(obj.when);

  for (const name of keys) {
    const wv: WhenValue = obj.when[name];

    const v: WhenValue = row[name] as WhenValue;

    if (typeof wv === 'boolean') {
      if (wv !== v) return true;
      continue;
    }

    console.log(wv);

    if (!wv) continue;

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!wv.includes(v)) return true;
  }

  return false;
}

/**
 * 查找工具
 * @return T arr[ values[index] = row[key] ]
 */
export const findByIndex: <T>(arr: Array<T>, key: string, row?: Row) => T = (arr, key, row) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return arr[row?.[key] || 0] ?? arr[0];
};
