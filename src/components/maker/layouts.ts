// const YmMakerLayoutTable = defineAsyncComponent(() => import('./layout/YmMakerLayoutTable.vue'));
// const YmMakerLayoutToolbar = defineAsyncComponent(
//   () => import('./layout/YmMakerLayoutToolbar.vue')
// );
// const YmMakerLayoutTag =
// const YmMakerLayoutForm = defineAsyncComponent(() => import('./layout/FormLayout.vue'));

import { defineAsyncComponent } from 'vue';
import type { DefineComponent } from 'vue';

const layouts = {
  tag: defineAsyncComponent(() => import('./layout/TagLayout.vue')),
  toolbar: defineAsyncComponent(() => import('./layout/ToolbarLayout.vue')),
  table: defineAsyncComponent(() => import('./layout/TableLayout.vue')),
  form: defineAsyncComponent(() => import('./layout/FormLayout.vue'))
};

export type MakerNames = keyof typeof layouts;

export const getLayout = (name: MakerNames) => (layouts[name] || layouts.tag) as DefineComponent;
