import mitt from 'mitt';

export const PropertyPanelOpen = Symbol('property-panel-open');
export const PropertyPanelClose = Symbol('property-panel-close');

const panelEvent = mitt();

export type PanelListener = () => void;

export const Panel = {
  onOpen: (fn: PanelListener) => panelEvent.on(PropertyPanelOpen, fn),
  onClose: (fn: PanelListener) => panelEvent.on(PropertyPanelClose, fn),
  open: () => panelEvent.emit(PropertyPanelOpen),
  close: (fn1?: PanelListener) => {
    panelEvent.emit(PropertyPanelClose);

    if (fn1) panelEvent.off(PropertyPanelClose, fn1);
    // if (fn2) panelEvent.removeListener(PropertyPanelOpen, fn2);
  }
};
