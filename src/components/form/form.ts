/**
 * # 表单文件
 */
import { DisabledComponent, Er, Row } from '../elementui';
import { Toolbar } from '../button/button';
import { InnerWidget, WidgetName } from './widgets';
import { FormItemRule, FormRules } from 'element-plus';

/**
 * ## 表单
 */
export interface Form extends Er {
  /**
   * 表单域列表
   */
  fields: Array<Field>;
  /**
   * 校验信息
   */
  rules?: FormRules;

  /**
   * 表单宽度
   */
  style?: Record<string, unknown>;
  /**
   * 标签宽度
   */
  labelWidth?: string;
  /**
   * 标签位置
   */
  labelPosition?: string;

  /**
   * 绑定的数据
   */
  row?: Row;

  /**
   * 提交按钮 string: Url
   */
  submit?: string;
  /**
   * 重置按钮
   */
  reset?: string | false;
  /**
   * 操作按钮
   */
  toolbar?: Toolbar['items'];
  /**
   * 填表说明
   */
  description?: string;
}

/**
 * ## 有选择项的控件
 */
export type Selector = 'select' | 'radio' | 'checkbox';

/**
 * 选择项配置
 */
export interface SelectorOption {
  value: string | number;
  label: string | number;
}

/**
 * 选择项配置组
 */
export type SelectorOptions = Array<SelectorOption>;

/**
 * ## 域关联
 */
export interface Trigger {
  /**
   * [p: me.value]: { [px: string]: value }
   */
  options?: { [p: string]: { [p: string]: unknown } };
}

/**
 * ## 表单域
 */
export interface Field extends DisabledComponent, Er {
  /**
   * 组件名称
   */
  widget?: WidgetName | InnerWidget;
  /**
   * 校验信息
   */
  rules?: FormItemRule | FormItemRule[];
  /**
   * 默认值
   */
  default?: unknown;

  /**
   * 子表单域
   */
  fields?: Field[];

  /**
   * options 选项
   */
  options?: SelectorOptions;
  /**
   * options 选项多选
   */
  multi?: boolean;

  /**
   * 组
   */
  arrayed?: boolean;

  /**
   * 重复
   */
  repeat?: boolean;

  /**
   * 关联域数据自动填充
   */
  trigger?: Trigger;

  /**
   * row模式下的宽度
   */
  col?: number;

  required?: boolean;

  min?: number;
  max?: number;
  step?: number;
  showSteps?: boolean;

  activeColor?: string;
  inactiveColor?: string;

  // [p: string]: unknown;
}

/**
 * ## 控件
 */
export interface Widget extends Er {
  name: WidgetName | 'group';
  /**
   * select/radio/checkbox options列表生成助手
   */
  onCreate?: Selector;

  [p: string]: unknown;
}

/**
 * 下拉选择类控件基础定义
 */
const select: Widget = {
  name: 'select',
  onCreate: 'select'
};

/**
 * ### 控件定义
 */
export const widgets: Record<WidgetName | 'group', Widget> = {
  cascader: {
    label: '级联菜单',
    name: 'cascader',
    icon: 'icon-cascader'
  },
  checkbox: {
    label: '多项选择',
    icon: 'icon-checkbox',
    name: 'checkbox',
    onCreate: 'checkbox'
  },
  select: {
    ...select,
    icon: 'icon-select',
    label: '下拉菜单'
  },
  radio: {
    label: '单项选择',
    icon: 'icon-radio',
    name: 'radio',
    onCreate: 'radio'
  },
  relation: {
    label: '关联',
    icon: 'icon-relation',
    name: 'relation'
  },
  color: {
    label: '色彩选择',
    name: 'color'
  },
  pair: {
    label: '选项组',
    name: 'pair'
  },
  file: {
    label: '文件上传',
    icon: 'icon-download',
    name: 'file'
  },
  date: {
    label: '日期选择',
    icon: 'icon-date',
    name: 'date'
  },
  number: {
    icon: 'icon-slider',
    label: '滑块',
    name: 'number'
  },
  time: {
    icon: 'icon-time',
    label: '时间选择',
    name: 'time'
  },
  switch: {
    label: '开关',
    icon: 'icon-switch',
    name: 'switch'
  },
  rate: {
    icon: 'icon-rate',
    label: '五星评价',
    name: 'rate'
  },
  input: {
    icon: 'icon-input',
    label: '单行文本',
    name: 'input'
  },
  text: {
    icon: 'icon-textarea',
    label: '多行文本',
    type: 'textarea',
    block: true,
    autosize: {
      minRows: 2,
      maxRows: 6
    },
    name: 'text'
  },
  group: {
    icon: 'el-group',
    label: '子表单',
    fields: [],
    name: 'group'
  }
};

/**
 * 获取组件默认值
 * @param {Field} field
 */
export function getDefaultValue(field: Field) {
  if (field.default !== undefined) return field.default;
  if (field.fields) return {};
  return undefined;
}
