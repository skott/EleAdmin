import { Field, getDefaultValue, Trigger } from '../form';
import { FunctionalComponent, h, PropType, Slot, VNode } from 'vue';
import { isDisabled, Row } from '../../elementui';
import FieldWidget from '../FieldWidget.vue';
import { FormItemRule } from 'element-plus';

/**
 * 包裹器: 将节点转为生成函数
 */
export const functional: <T>(n: T) => () => T = (n) => () => n;

/**
 * 虚拟结点
 */
export type YNode = VNode | string | number | undefined;

/**
 * 事件
 */
export type FormEvents = {
  /**
   * 表单数据更新事件
   */
  modelValue?: (v?: Row) => void;
  /**
   * 表单结构更新事件(用于fields)
   */
  elements?: (v?: Array<Field>) => void;
  /**
   * 表单域选中事件
   */
  select?: (index?: number) => void;
  /**
   * 表单域删除事件
   */
  remove?: (index?: number) => void;
};

export type FieldEvents = {
  /**
   * 表单数据更新事件
   */
  modelValue?: (v: unknown) => void;
  /**
   * 表单域更新事件
   */
  element?: (v: Field) => void;
  /**
   * 表单域选中事件
   */
  select?: () => void;
  /**
   * 表单域删除事件
   */
  remove?: () => void;
};

/**
 * 上下文
 */
export declare interface WrapperContext {
  slots?: { core?: Slot; label?: Slot };
  wrappers?: {
    labelWrapper?: Wrapper<FieldEvents>;
    groupWrapper: Wrapper<FieldEvents>;
    coreWrapper: Wrapper<FieldEvents, Field, YNode[]>;
    arrayWrapper: Wrapper<FieldEvents>;
    mainWrapper: Wrapper<FormEvents, Field[]>;
  };
}

/**
 * 参数
 */
export declare type WrapperParam<E, F = Field> = {
  /**
   * 表单域
   */
  field: F;
  /**
   * 表单域索引
   */
  index?: number;
  /**
   * 层级， 记录每层的索引
   */
  // level?: number[];
  /**
   * 值
   */
  value?: Row | unknown;
  /**
   * 事件
   */
  events?: E;
  /**
   * 不变上下文
   */
  context?: WrapperContext;
  /**
   * 可变参数
   */
  attrs?: { [p: string]: unknown };
};

/**
 * 包装函数
 */
export declare type Wrapper<E, F = Field, R = YNode[] | undefined> = (
  param: WrapperParam<E, F>
) => R;

/**
 * 处理 repeat 属性
 */
const getRepeatRulesOf = (v: unknown) => {
  const error = '两次输入不一致!';
  return [
    {
      trigger: 'change',
      validator: (rule, value, callback) => {
        if (v !== value) {
          callback(error);
          return false;
        } else {
          callback();
          return true;
        }
      }
    }
  ] as FormItemRule[];
};

/**
 * 处理联动 if (my-value === me) mixin(trigger.options[me])
 *
 * @param { Trigger } trigger 联动定义
 * @param { string | number } me
 * @param newValue
 */
const fnTrigger = (trigger: Trigger, me: string | number, newValue: Record<string, unknown>) => {
  const v = trigger.options ? trigger.options[me] : {};

  return {
    ...newValue,
    ...v
  };
};

/**
 * 数组化表单域事件
 */
export interface ArrayNodeEvents {
  _array_add: () => void;
  _array_remove: (index: number) => void;
  _array_change: (v: unknown, index: number) => void;
}

/**
 * 分支函数
 */
export const creator: Wrapper<FieldEvents> = (param) => {
  const { field, context } = param;

  if (field.arrayed) return context?.wrappers?.arrayWrapper(param);

  if (field.fields) return context?.wrappers?.groupWrapper(param);

  return context?.wrappers?.coreWrapper(param);
};

/**
 * Group/Arrayed 的 label
 */
export const labelCreator: Wrapper<FieldEvents> = (param) => {
  const { field, context } = param;
  if (context?.slots?.label) return context.slots.label(param);
  if (context?.wrappers?.labelWrapper) return context.wrappers.labelWrapper(param);
  return [field.label];
};

/**
 * 表单域渲染函数
 */
export const coreCreator: Wrapper<FieldEvents, Field, YNode[]> = (param) => {
  const { field, value, events } = param;
  // TODO 这里去掉了 inline 和 label-position 的传入，希望通过 注入 处理
  const props_ = {
    ...field
  };

  const nodes: Array<VNode> = [];

  if (field.repeat) {
    let v: unknown;
    nodes.push(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      h(FieldWidget, {
        ...props_,
        modelValue: v,
        'onUpdate:modelValue': (v_: unknown) => (v = v_),
        rules: getRepeatRulesOf(value)
      })
    );
  }

  nodes.push(
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    h(FieldWidget, {
      ...props_,
      modelValue: value,
      'onUpdate:modelValue': events?.modelValue
    })
  );

  return nodes;
};

/**
 * fields渲染函数
 */
export const mainCreator: Wrapper<FormEvents, Field[]> = (param) => {
  const { field: fields, events, context, value = {} } = param;

  let children: YNode[] = [];

  const shadow: Field[] = [...(fields || [])];

  fields.forEach((f: Field, index: number) => {
    // disabled
    if (isDisabled(f, value as Row)) return;

    // 处理 model-value 并 trigger
    const trigger = (v: unknown) => {
      let newValue = {
        ...(value as Row),
        [f.name]: v
      };

      const type = typeof v;

      if (f.trigger && (type === 'number' || type === 'string')) {
        newValue = fnTrigger(f.trigger, v as number | string, newValue);
      }

      events?.modelValue?.(newValue);
    };

    const field = {
      ...f,
      repeat: undefined
    };

    const handler: FieldEvents = {
      modelValue: trigger,
      element: (v?: Field) => {
        if (v) {
          shadow.splice(index, 1, v);
        } else {
          shadow.splice(index, 1);
        }
        (events as FormEvents)?.elements?.(shadow);
      },
      select: () => events?.select?.(index),
      remove: () => events?.remove?.(index)
    };

    const nodes = creator({
      ...param,
      field,
      value: (value as Row)[f.name],
      events: handler,
      index
    });

    if (nodes) children = children.concat(nodes);

    if (f.repeat && !f.arrayed) {
      const nodes = creator({ field: f, value: (value as Row)[f.name], context });
      if (nodes) children = children.concat(nodes);
    }
  });

  return children;
};

/**
 * arrayed 渲染函数
 */
export const arrayCreator: Wrapper<
  FieldEvents,
  Field,
  { handler: ArrayNodeEvents; children: Array<YNode[] | undefined> }
> = (param) => {
  const { field, value, events } = param;

  const shadow = [...((value as Array<unknown>) || [])];

  /**
   * 数组操作
   */
  const handler: ArrayNodeEvents = {
    _array_add: () => {
      shadow.push(getDefaultValue(field));
      events?.modelValue?.(shadow);
    },
    _array_remove: (index: number) => {
      shadow.splice(index, 1);
      events?.modelValue?.(shadow);
    },
    _array_change: (v: unknown, index: number) => {
      if (index === undefined) return;
      shadow.splice(index, 1, v);
      events?.modelValue?.(shadow);
    }
  };

  const children =
    (value as Array<unknown>)?.map((v: unknown, index: number) =>
      creator({
        ...param,
        field: {
          ...field,
          arrayed: false,
          label: undefined
        },
        value: v,
        events: {
          ...(events || {}),
          modelValue: (v_?: unknown) => {
            handler._array_change(v_, index);
          }
        }
      })
    ) || [];

  return { handler, children };
};

/**
 * widget.fields 渲染
 */
export const groupCreator: Wrapper<FieldEvents> = (param) => {
  const { field, events, context } = param;

  const shadow: Field = { ...(field || {}) };
  const handler: FormEvents = {
    modelValue: events?.modelValue,
    elements: (v?: Field[]) => {
      shadow.fields = v;
      events?.element?.(shadow);
    }
  };

  // const level_: number[] = level || [];
  // level_.push(index ?? 0);

  const mainWrapper = context?.wrappers?.mainWrapper || mainCreator;
  return mainWrapper({ ...param, field: field.fields || [], events: handler });
};

/**
 * Component-able
 */
export const getLayoutComponent: (
  fn: FunctionalComponent<{ fields?: Field[]; modelValue?: Row }>
) => FunctionalComponent = (fn) => {
  fn.props = {
    fields: Array as PropType<Field[]>,
    modelValue: {} as PropType<Row>
  };
  fn.inheritAttrs = false;

  return fn;
};
