import { defineAsyncComponent } from 'vue';

const Buttons = {
  dialog: defineAsyncComponent(() => import('./CommandButton.vue')),
  switch: defineAsyncComponent(() => import('../icon/SwitchIcon.vue')),
  common: defineAsyncComponent(() => import('./SimpleButton.vue'))
};

export type ButtonName = keyof typeof Buttons;
export const getButton = (name: ButtonName) => Buttons[name] || Buttons.common;
