import { MockMethod } from 'vite-plugin-mock';
import { SeriesName } from '../src/components/maker/structure';
import { View } from '../src/components/core';

export default [
  {
    url: '/api/maker/model',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Maker',
          series: 'model' as SeriesName,
          url: 'api/maker/upload'
        } as View
      };
    }
  },
  {
    url: '/api/maker/form',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Maker',
          series: 'form' as SeriesName,
          url: 'api/maker/upload'
        } as View
      };
    }
  },
  {
    url: '/api/maker/table',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Maker',
          series: 'table' as SeriesName,
          url: 'api/maker/upload'
        } as View
      };
    }
  },
  {
    url: '/api/maker/module',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'M2',
          submit: 'api/maker/upload',
          url: 'api/module/name'
        } as View
      };
    }
  },
  {
    url: '/api/table/upload',
    method: 'post',
    response: () => {
      return {};
    }
  }
] as MockMethod[];
